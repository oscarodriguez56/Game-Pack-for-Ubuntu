#!/bin/bash


sudo add-apt-repository ppa:lutris-team/lutris
sudo apt-get update
sudo apt-get install lutris -y 


sudo apt install flatpak -y

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak install flathub com.usebottles.bottles


sudo apt install dosbox -y

sudo add-apt-repository ppa:ppsspp/stable
sudo apt-get update
sudo apt-get install psspp -y 

sudo apt install dsmume -y

wget https://steamcdn-a.akamaihd.net/client/installer/steam.deb


sudo dpkg -i steam.deb

sudo apt install nestopia -y
